<?php 
session_start();

require_once 'includes/functions.inc.php';

if(!isset($_SESSION['email']) || $_SESSION['email'] == '') {
    die('No ha iniciado sesión');
}

if(isset($_GET['cancel']) && $_GET['cancel'] == true){
    $_SESSION['error'] = 'No se ha añadido nada';
    header("Location: autos.php");
    return;
}

require_once 'models/Auto.php';
$autos = new Auto();
$autos->makeConnection();


if ($_SERVER['REQUEST_METHOD'] === 'POST'){
    if(!is_numeric($_POST['mileage'])){
        $_SESSION['error'] = 'Kilometraje y año deben de ser numéricos';
        header("Location: add.php");
        return;
    } else {
        $mileage = htmlentities($_POST['mileage'], ENT_SUBSTITUTE, 'UTF-8', true);
    }
    if(!is_numeric($_POST['year'])){
        $_SESSION['error'] = 'Kilometraje y año deben de ser numéricos';
        header("Location: add.php");
        return;
    } else {
        $year = htmlentities($_POST['year'], ENT_SUBSTITUTE, 'UTF-8', true);
    }
    if(!isset($_POST['make']) || trim($_POST['make'], ' ') == ''){
        $_SESSION['error'] = 'El campo marca es obligatorio';
        header("Location: add.php");
        return;
    } else {
        $make = htmlentities($_POST['make'], ENT_SUBSTITUTE, 'UTF-8', true);
    }

    if(!isset($_SESSION['error']) || $_SESSION['error'] == ''){
        $autos->setMake($make);
        $autos->setMileage($mileage);
        $autos->setYear($year);
        $autos->addAuto($autos);

        $_SESSION['success'] = 'Vehículo añadido correctamente';
        header("Location: autos.php");
        return;
    }    
}

require_once 'views/add.view.php';
?>