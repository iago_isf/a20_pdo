<?php
session_start();

if(!isset($_SESSION['email']) || $_SESSION['email'] == '') {
    die('No ha iniciado sesión');
}

require_once 'models/Auto.php';
$autos = new Auto();
$autos->makeConnection();

if(isset($_GET['id'])){ 
    $id = $_GET['id'];
} else { 
    die('Algo salió mal');
}

if(isset($_GET['delete'])){
    if($_GET['delete'] == 'no'){
        $_SESSION['error'] = 'Vehículo no borrado';
        header("Location: autos.php");
        return;
    } else {
        $autos->delAuto($_GET['delete']);

        $_SESSION['success'] = 'Vehículo eliminado correctamente';
        header("Location: autos.php");
        return;
    }
}

$eAuto = $autos->getAuto($id);
$oMake = $eAuto->getMake();
$oMileage = $eAuto->getMileage();
$oYear = $eAuto->getYear();

require_once 'views/del.view.php';
?>