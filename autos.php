<?php
session_start();

require_once 'includes/functions.inc.php';

if(isset($_GET['logout'])){
    header('Location: index.php');
    die();
}

if(!isset($_SESSION['email']) || $_SESSION['email'] == '') {
    die('No ha iniciado sesión');
}

require_once 'models/Auto.php';
$autos = new Auto();
$autos->makeConnection();

require_once 'views/autos.view.php';
?>