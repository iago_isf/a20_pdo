<?php
    function labelRed($text){
        return '<label class="bg-danger bg-gradient bg-opacity-75 border border-danger text-white rounded p-2">'.$text.'</label>';
    }

    function labelGreen($text){
        return '<label class="bg-success bg-gradient bg-opacity-75 border border-success text-white rounded p-2">'.$text.'</label>';
    }
?>