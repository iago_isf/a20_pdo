<?php
    require 'parts/head.part.php';
?>
<header class="container">
    <div class="row justify-content-center">
        <div class="col col-5 text-center">
            <h1>Editar vehículo:</h1>
        </div>
    </div>
</header>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-6 text-center">
            <form method="POST">
                <div class="row justify-content-center">
                    <div class="col col-2">
                        Marca:
                    </div>
                    <div class="col col-6">
                        <input type="text" class="form-control" name="make" id="make">
                    </div>
                </div>
                <div class="row my-3 justify-content-center">
                    <div class="col col-2">
                        Kilómetros:
                    </div>
                    <div class="col col-6">
                        <input type="number" class="form-control" name="mileage" id="mileage">
                    </div>
                </div>
                <div class="row my-3 justify-content-center">
                    <div class="col col-2">
                        Año:
                    </div>
                    <div class="col col-6">
                        <input type="number" class="form-control" name="year" id="year">
                    </div>
                </div>
                <?php if(isset($_SESSION['error'])) echo labelRed($_SESSION['error']); unset($_SESSION['error']);?>
                <div class="row my-3 justify-content-center text-center">
                    <div class="col col-2 my-2"><input type="submit" value="Añadir" class="btn btn-outline-warning"></div>
                    <div class="col col-2 my-2"><a href="add.php?cancel=true" class="btn btn-warning">Cancelar</a></div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
    require_once 'parts/footer.part.php';
?>