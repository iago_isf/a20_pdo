<?php
    require 'parts/head.part.php';
?>
    <div class="container">
        <h1>Inicie sesión</h1>
        <form action="" method="POST">
            <?php if(isset($_SESSION['error'])) echo labelRed($_SESSION['error']); unset($_SESSION['error']);?>
            <p>Correo: <input type="email" name="email" id="email"></p>
            <p>Contraseña: <input type="password" name="pass" id="pass"></p>
            <p><input type="submit" value="Iniciar sesión" class="btn btn-warning"></p>
        </form>
    </div>

<?php
    require_once 'parts/footer.part.php';
?>