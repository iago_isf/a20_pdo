<?php
    require 'parts/head.part.php';
?>

<header class="container mt-4">
    <div class="row justify-content-evenly">
        <div class="col col-5 text-center">
            <h3>Ha iniciado sesión como:</h3>
            <h6><?= $_SESSION['email'] ?></h6>
        </div>
        <div class="col col-3 align-self-center text-center"><a href="logout.php" class="btn btn-danger">Cerrar sesión</a></div>
    </div>
</header>
<div class="container mt-4">
    <div class="row text-center">
        <div class="col">
            <a href="add.php" class="btn btn-info text-white border border-primary rounded-pill w-50">Añadir un vehículo</a>
        </div>
    </div>
    <div class="row justify-content-center mt-4">
        <div class="col text-center">
        <?php
            if(isset($_SESSION['success'])) echo labelGreen($_SESSION['success']); unset($_SESSION['success']);
            if(isset($_SESSION['error'])) echo labelRed($_SESSION['error']); unset($_SESSION['error']);
        ?>
        </div>
    </div>
    <div class="row">
        <div class="col col-10">
        <div class="row">
        <?php  
        if ($autos->getNumAutos() >= 0){
            $tAutos = $autos->getAutos();
            foreach($tAutos as $auto) {
        ?>
            <div class="col col-3 mt-4">
                <div class="card">
                    <div class="card-body">
                        <div class="my-2">Marca: <?= $auto->getMake(); ?></div>
                        <div class="my-2">Kilómetros: <?= $auto->getMileage(); ?></div>
                        <div class="my-2">Año: <?= $auto->getYear(); ?></div>
                        <div class="my-2 row justify-content-evenly">
                            <div class="col text-center">
                                <a href="edit.php?id=<?= $auto->auto_id ?>" class="btn btn-outline-warning">Editar</a>
                            </div>
                            <div class="col text-center">
                                <a href="del.php?id=<?= $auto->auto_id ?>" class="btn btn-outline-danger">Eliminar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
            }
        } else {
            echo '<h5 class="text-center mt-4">No se encontraron filas</h5>';
        }
        ?>
        </div>
        </div>
        <div class="col-2 mt-4">
            <h3 class="text-center">Registros:</h3>
            <h6 class="text-center"><?= $autos->getNumAutos(); ?></h6>
        </div>
    </div>
</div>

<?php
    require_once 'parts/footer.part.php';
?>