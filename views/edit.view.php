<?php
    require 'parts/head.part.php';
?>

<header class="container">
    <div class="row justify-content-center">
        <div class="col col-5 text-center">
            <h1>Editar vehículo:</h1>
        </div>
    </div>
</header>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-6 text-center">
            <form action="edit.php?id=<?= $id; ?>" method="POST">
                <h5>ID del vehículo: <?= $id; ?></h5>
                <div class="row justify-content-center">
                    <div class="col col-2">
                        Marca:
                    </div>
                    <div class="col col-6">
                        <input type="text" class="form-control" name="make" id="make" value="<?= $oMake; ?>">
                    </div>
                </div>
                <div class="row my-3 justify-content-center">
                    <div class="col col-2">
                        Kilómetros:
                    </div>
                    <div class="col col-6">
                        <input type="number" class="form-control" name="mileage" id="mileage" value="<?= $oMileage; ?>">
                    </div>
                </div>
                <div class="row my-3 justify-content-center">
                    <div class="col col-2">
                        Año:
                    </div>
                    <div class="col col-6">
                        <input type="number" class="form-control" name="year" id="year" value="<?= $oYear; ?>">
                    </div>
                </div>
                <?php if(isset($_SESSION['error'])) echo labelRed($_SESSION['error']); unset($_SESSION['error']);?>
                <div class="row my-3 justify-content-center text-center">
                    <div class="col col-2 my-2"><input type="submit" value="Editar" class="btn btn-outline-warning"></div>
                    <div class="col col-2 my-2"><a href="edit.php?id=<?= $id ?>&cancel=true" class="btn btn-warning">Cancelar</a></div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
    require_once 'parts/footer.part.php';
?>