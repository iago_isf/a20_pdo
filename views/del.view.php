<?php
    require 'parts/head.part.php';
?>
<div class="container text-center mt-5">
    <h5>ID del vehículo: <?= $id; ?></h5>
    <div class="my-2">Marca: <?= $oMake; ?></div>
    <div class="my-2">Kilómetros: <?= $oMileage; ?></div>
    <div class="my-2">Año: <?= $oYear; ?></div>
   
    <div class="row justify-content-center mt-5">
        <div class="col col-2 text-center">
            <a href="del.php?delete=<?= $id ?>" class="btn btn-outline-danger">Eliminar</a>
        </div>
        <div class="col col-2 text-center">
            <a href="del.php?delete=no" class="btn btn-danger">Cancelar</a>
        </div>
    </div>
</div>
<?php
    require_once 'parts/footer.part.php';
?>