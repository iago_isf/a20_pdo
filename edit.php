<?php
session_start();

require_once 'includes/functions.inc.php';

if(!isset($_SESSION['email']) || $_SESSION['email'] == '') {
    die('No ha iniciado sesión');
}

if(isset($_GET['cancel']) && $_GET['cancel'] == true){
    $_SESSION['error'] = 'No se ha editado nada';
    header("Location: autos.php");
    return;
}

if(isset($_GET['id'])){ 
    $id = $_GET['id'];
} else { 
    die('Algo salió mal');
}

require_once 'models/Auto.php';
$autos = new Auto();
$autos->makeConnection();

if ($_SERVER['REQUEST_METHOD'] === 'POST'){
    $nMake = htmlentities($_POST['make'], ENT_SUBSTITUTE, 'UTF-8', true);
    $nMileage = htmlentities($_POST['mileage'], ENT_SUBSTITUTE, 'UTF-8', true);
    $nYear = htmlentities($_POST['year'], ENT_SUBSTITUTE, 'UTF-8', true);

    if(trim($nMake, ' ') != '' && trim($nMileage, ' ') != '' && trim($nYear, ' ') != ''){
        $autos->setMake($nMake);
        $autos->setMileage($nMileage);
        $autos->setYear($nYear);
        $autos->editAuto($autos, $id);

        $_SESSION['success'] = 'Registro modificado';
        header("Location: autos.php");
        return;
    } else {
        $_SESSION['error'] = 'Completa los campos de forma correcta';
        header("Location: edit.php?id=$id");
        return;
    }
}

$eAuto = $autos->getAuto($id);
$oMake = $eAuto->getMake();
$oMileage = $eAuto->getMileage();
$oYear = $eAuto->getYear();

require_once 'views/edit.view.php';
?>