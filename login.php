<?php
    session_start();

    include_once 'includes/functions.inc.php';

    if ($_SERVER['REQUEST_METHOD'] === 'POST'){
        $salt = 'XyZzy12*_';
        $stored_hash = '1a52e17fa899cf40fb04cfc42e6352f1';

        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $pass = isset($_POST['pass']) ? $salt.$_POST['pass'] : '';

        if($email == '' && $pass == '' || $pass == $salt || !filter_var($email, FILTER_VALIDATE_EMAIL)){
            $_SESSION['error'] = 'Se requiere un nombre de usuario y una contraseña válidos para acceder';
            header("Location: login.php");
            return;
        }
        else if (hash('md5', $pass) != $stored_hash) {
            $check = md5($pass);
            error_log('Login fail'.$_POST['email'].$check);

            $_SESSION['error'] = 'Contraseña incorrecta';
            header("Location: login.php");
            return;
        } 
        else {
            error_log('Login success'.$_POST['email']);
            
            $_SESSION['email'] = $_POST['email'];
            header("Location: autos.php");
            return;
        } 
    }

    require 'views/login.view.php';
?>