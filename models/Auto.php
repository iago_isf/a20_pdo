<?php 
    require_once 'lib/Database.php';

    class Auto
    {
        private $db;

        private $make;
        private $year;
        private $mileage;
        
        public function __construct($make = '', $year = 0, $mileage = 0)
        {
            $this->make = $make;
            $this->year = $year;
            $this->mileage = $mileage;
        }
        
        public function makeConnection()
        {
            $this->db = new Database();
        }

        // GETTERS Y SETTERS
        public function getDb()
        {
            return $this->db;
        }
        public function setDb($db): self
        {
            $this->db = $db;
            return $this;
        }

        public function getMake()
        {
            return $this->make;
        }
        public function setMake($make): self
        {
            $this->make = $make;
            return $this;
        }

        public function getYear()
        {
            return $this->year;
        }
        public function setYear($year): self
        {
            $this->year = $year;
            return $this;
        }

        public function getMileage()
        {
            return $this->mileage;
        }
        public function setMileage($mileage): self
        {
            $this->mileage = $mileage;
            return $this;
        }

        // METODOS DE LA CLASE
        public function getAutos()
        {
            $this->db->query("SELECT auto_id, make, year, mileage FROM autos");
            $results = $this->db->resultSet('Auto');
            return $results;
        }

        public function getAuto($id)
        {
            $this->db->query("SELECT make, year, mileage FROM autos WHERE auto_id=:id");
            $this->db->bind(':id', $id);
            $results = $this->db->single('Auto');
            return $results;
        }

        public function addAuto($auto)
        {
            $this->db->query("INSERT INTO autos (make, year, mileage) VALUES (:mk,:yr,:mi)");
            $this->db->bind(':mk', $auto->getMake());
            $this->db->bind(':yr', $auto->getYear());
            $this->db->bind(':mi', $auto->getMileage());
            $this->db->execute();
        }

        public function editAuto($auto, $id)
        {
            $this->db->query("UPDATE autos SET make=:mk, mileage=:mi, year=:yr WHERE auto_id=:id");
            $this->db->bind(':id', $id);
            $this->db->bind(':mk', $auto->getMake());
            $this->db->bind(':yr', $auto->getYear());
            $this->db->bind(':mi', $auto->getMileage());
            $this->db->execute();
        }

        public function delAuto($id)
        {
            $this->db->query("DELETE FROM autos WHERE auto_id = :id");
            $this->db->bind(':id', $id);
            $this->db->execute();
        }

        public function getNumAutos()
        {
            $this->db->query("SELECT * FROM autos");
            return $this->db->getRows();
        }
    }
?>